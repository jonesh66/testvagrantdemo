****TestVagrant Demo****

Project to demonstrate a basic fetch-author-books functionality using the Goodreads API.

Project Checklist:

*[x] Add gradle to the project to manage dependencies.
*[x] Add dependencies for:
    1. RestAssured
    2. Selenium-Java
    3. Jackson XML Databind
*[x] Add tests to cover functionalities:
    1. Search a user based on keyword
    2. Search for all books released by said author.
*[ ] Add support for reporting.

**Running the project**

Pull the project and start a gradle build by invoking
`./gradlew build`

Run the tests by invoking
`./gradlew clean test`


