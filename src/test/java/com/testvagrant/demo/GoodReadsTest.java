package com.testvagrant.demo;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.*;
import java.io.IOException;
import java.io.StringReader;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static io.restassured.RestAssured.given;

public class GoodReadsTest {

    private static final String API_KEY = "yG3myjFzaP8VyassMNwQ";
    private static RequestSpecification requestSpecification;
    private static String baseURI = "https://www.goodreads.com";
    String authorName = "james";
    String authorId;

    @Test
    public void setAuthor() throws IOException, ParserConfigurationException, SAXException {
        String resource = "/api/author_url";
        authorName = "/" + authorName;
        baseURI += resource;
        String request = baseURI + authorName;

        requestSpecification = new RequestSpecBuilder()
                .setBaseUri(baseURI)
                .build();

        Response response = given()
                .spec(requestSpecification)
                .when()
                .param("key", API_KEY)
                .get(request)
                .then()
                .extract()
                .response();

        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();

        DefaultHandler handler = new SaxHandler();

        parser.parse(new InputSource(new StringReader(response.asString())), handler);
        System.out.println("Author ID: " + authorId);


    }

    @Test(dependsOnMethods = {"setAuthor"})
    public void getListOfBooksByAuthorName() throws ParserConfigurationException, IOException, SAXException {
        baseURI = "https://www.goodreads.com";
        String bookResource = "/author/show/";
        String booksListBaseURI = baseURI + bookResource;

        requestSpecification = new RequestSpecBuilder()
                .setBaseUri(booksListBaseURI)
                .build();


        String booksListRequest = booksListBaseURI + authorId;
        System.out.println(String.format("Books list request: %s", booksListRequest));
        Response response = given()
                .spec(requestSpecification)
                .when()
                .param("key", API_KEY)
                .get(booksListRequest)
                .then()
                .extract()
                .response();

        Document document = getDocument(response);
        NodeList booksList = document.getElementsByTagName("title_without_series");

        Stream<Node> nodeStream = IntStream.range(0, booksList.getLength())
                .mapToObj(booksList::item);

        nodeStream.forEach(item -> System.out.println(item.getTextContent()));

    }

    private Document getDocument(Response response) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        InputSource inputSource = new InputSource(new StringReader(response.asString()));
        return documentBuilder.parse(inputSource);
    }

    class SaxHandler extends DefaultHandler {
        /**
         * Receive notification of the start of an element.
         *
         * <p>By default, do nothing.  Application writers may override this
         * method in a subclass to take specific actions at the start of
         * each element (such as allocating a new tree node or writing
         * output to a file).</p>
         *
         * @param uri        The Namespace URI, or the empty string if the
         *                   element has no Namespace URI or if Namespace
         *                   processing is not being performed.
         * @param localName  The local name (without prefix), or the
         *                   empty string if Namespace processing is not being
         *                   performed.
         * @param qName      The qualified name (with prefix), or the
         *                   empty string if qualified names are not available.
         * @param attributes The attributes attached to the element.  If
         *                   there are no attributes, it shall be an empty
         *                   Attributes object.
         * @see ContentHandler#startElement
         */
        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) {
            int attributeLength = attributes.getLength();
            System.out.println("Attribute length: " + attributeLength);
            if ("author".equals(qName) && attributes.getQName(0).equals("id")) {
                authorId = attributes.getValue(0);

                System.out.println("Author ID: " + authorId);
            }
        }
    }

}
