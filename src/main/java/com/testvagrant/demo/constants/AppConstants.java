package com.testvagrant.demo.constants;

public class AppConstants {
    public static final String CORRECT_CARD_NUMBER = "4811111111111114";
    public static final String INCORRECT_CARD_NUMBER = "4811111111111113";
    public static final String EXPIRY_DATE = "02/21";
    public static final String CVV_NUMBER = "123";
    public static final String OTP = "112233";
}
