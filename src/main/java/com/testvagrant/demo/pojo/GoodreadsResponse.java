package com.testvagrant.demo.pojo;


public class GoodreadsResponse
{
    private Author author;

    private Request Request;

    public Author getAuthor ()
    {
        return author;
    }

    public void setAuthor (Author author)
    {
        this.author = author;
    }

    public Request getRequest ()
    {
        return Request;
    }

    public void setRequest (Request Request)
    {
        this.Request = Request;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [author = "+author+", Request = "+Request+"]";
    }
}
