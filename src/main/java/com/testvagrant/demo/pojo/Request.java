package com.testvagrant.demo.pojo;

public class Request
{
    private String method;

    private String key;

    private String authentication;

    public String getMethod ()
    {
        return method;
    }

    public void setMethod (String method)
    {
        this.method = method;
    }

    public String getKey ()
    {
        return key;
    }

    public void setKey (String key)
    {
        this.key = key;
    }

    public String getAuthentication ()
    {
        return authentication;
    }

    public void setAuthentication (String authentication)
    {
        this.authentication = authentication;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [method = "+method+", key = "+key+", authentication = "+authentication+"]";
    }
}

