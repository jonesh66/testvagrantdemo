package com.testvagrant.demo;

import com.testvagrant.demo.modules.Pages;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class BaseTest extends Pages {

    protected static List<DriverFactory> webdriverThreadPool = Collections.synchronizedList(new ArrayList<>());
    private static ThreadLocal<DriverFactory> driverFactoryThreadLocal;
    private final Properties properties = new Properties();

    public static synchronized RemoteWebDriver getDriver() {
        return driverFactoryThreadLocal.get().getDriver();
    }

    @BeforeSuite
    public void instantiateDriverObject() {
        driverFactoryThreadLocal = ThreadLocal.withInitial(() -> {
            DriverFactory driverFactory = new DriverFactory();
            webdriverThreadPool.add(driverFactory);
            return driverFactory;
        });
    }

    @AfterSuite
    public void closeDriverObjects() {
        webdriverThreadPool.forEach(DriverFactory::quitDriver);
        driverFactoryThreadLocal.remove();
    }


    @AfterMethod
    public void cleanUp() {
        driverFactoryThreadLocal.get().quitDriver();
    }

}
