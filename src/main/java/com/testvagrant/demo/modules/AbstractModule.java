package com.testvagrant.demo.modules;

public abstract class AbstractModule<T extends AbstractModule<T>> {
    public T get() {
        return (T) this;
    }
}
