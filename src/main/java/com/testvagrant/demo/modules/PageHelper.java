package com.testvagrant.demo.modules;

import com.testvagrant.demo.BaseTest;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;


public abstract class PageHelper {

    private final ConcurrentHashMap<RemoteWebDriver, ConcurrentHashMap<String, Object>> pageHashMap = new ConcurrentHashMap<>();

    private static <TPage extends AbstractModule<TPage>> TPage instantiateNewPage(RemoteWebDriver driver, Class<TPage> Clazz) {
        try {
            try {
                Constructor<TPage> constructor = Clazz.getConstructor(RemoteWebDriver.class);
                return constructor.newInstance(driver);
            } catch (NoSuchMethodException e) {
                return Clazz.getDeclaredConstructor().newInstance();

            }
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e1) {
            throw new RuntimeException(e1);
        }
    }

    private synchronized void setPageHashMap(RemoteWebDriver driver, String pageName, Object obj) {
        System.out.println("failed: Getting driver instance " + driver);
        if (!this.pageHashMap.containsKey(driver)) {
            ConcurrentHashMap<String, Object> pageHashMapPageNamePageObj = new ConcurrentHashMap<>();
            pageHashMapPageNamePageObj.put(pageName, obj);
            this.pageHashMap.put(driver, pageHashMapPageNamePageObj);
        } else {
            if (!this.pageHashMap.get(driver).containsKey(pageName)) {
                this.pageHashMap.get(driver).put(pageName, obj);
            }
        }
    }

    private synchronized Object getPageHashMap(RemoteWebDriver driver, String pageName) {
        if (this.pageHashMap.containsKey(driver)) {
            return this.pageHashMap.get(driver).get(pageName);
        } else {
            return null;
        }
    }

    protected synchronized <TPage extends AbstractModule<TPage>> TPage createPage(String pageName, Class<TPage> Clazz) {
        AtomicReference<TPage> tPage = new AtomicReference(getPageHashMap(BaseTest.getDriver(), pageName));

        if (null != tPage.get() && null != tPage.get()) {
            return tPage.get();
        } else {
            RemoteWebDriver driver = BaseTest.getDriver();
            tPage.set(instantiateNewPage(driver, Clazz));
            setPageHashMap(driver, pageName, tPage.get());
            return (TPage) getPageHashMap(driver, pageName);
        }
    }
}
