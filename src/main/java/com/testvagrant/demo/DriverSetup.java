package com.testvagrant.demo;

import org.openqa.selenium.remote.RemoteWebDriver;

public interface DriverSetup {

    RemoteWebDriver getWebdriver();
}
