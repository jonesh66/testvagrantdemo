package com.testvagrant.demo;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

public enum DriverType implements DriverSetup {

    CHROME {
        @Override
        public RemoteWebDriver getWebdriver() {
            ChromeOptions chromeOptions = new ChromeOptions();
            WebDriverManager.chromedriver().setup();
            return new ChromeDriver(chromeOptions);
        }
    },

    FIREFOX {
        @Override
        public RemoteWebDriver getWebdriver() {
            WebDriverManager.firefoxdriver().setup();
            FirefoxOptions firefoxOptions = new FirefoxOptions();
            return new FirefoxDriver(firefoxOptions);
        }
    }

}
