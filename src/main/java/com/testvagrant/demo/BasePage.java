package com.testvagrant.demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class BasePage {
    private static final int TIMEOUT = 30;
    private static final int POLLING = 100;

    protected static WebDriver driver;
    private WebDriverWait wait;

    public BasePage(WebDriver receivedDriver) {
        driver = receivedDriver;
        this.wait = new WebDriverWait(driver, TIMEOUT, POLLING);

    }

    protected void waitForElementToAppear(By locator) {
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    protected void waitForElementToDisappear(By locator) {
        this.wait.until((ExpectedConditions.invisibilityOfElementLocated(locator)));
    }

}