package com.testvagrant.demo;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class DriverFactory {

    public static URL gridURL;

    static {
        try {
            gridURL = new URL("http://192.168.0.109:4444/wd/hub");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private final String operatingSystem = System.getProperty("os.name").toLowerCase();
    private final String systemArchitecture = System.getProperty("os.arch");
    private final Boolean useRemoteWebDriver = Boolean.getBoolean("remoteWebDriver");
    private RemoteWebDriver driver;
    private ChromeOptions chromeOptions = new ChromeOptions();


    public RemoteWebDriver getDriver() {
        if (null == this.driver) {
            instantiateWebDriver();
        }
        return this.driver;
    }

    private void instantiateWebDriver() {
        System.out.println("*************************************************************");
        System.out.println("Local Operating System: " + this.operatingSystem);
        System.out.println("Local Architecture: " + this.systemArchitecture);
        System.out.println("Connecting to Selenium Grid: " + this.useRemoteWebDriver);
        System.out.println("*************************************************************");

        if (this.useRemoteWebDriver) {
            this.driver = new RemoteWebDriver(gridURL, this.chromeOptions);
            System.out.println("Starting webdriver using driver : " + this.driver.toString());
            this.driver.manage().window().maximize();
        }

    }

    public void quitDriver() {
        if (null != this.driver) {
            this.driver.quit();
        }

    }
}
